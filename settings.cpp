#include "settings.h"
#include "ui_settings.h"

#include <QDebug>
#include <QMessageBox>
#include <QProcess>
#include <QJsonDocument>
#include <QJsonObject>

Settings::Settings(QWidget* parent)
    : QDialog(parent)
    , ui(new Ui::Settings)
{
    ui->setupUi(this);
    QScopedPointer<QProcess> proc(new QProcess());
    proc->start("snapper", QStringList() << "--jsonout" << "get-config");
    if (!proc->waitForFinished()) {
        errorMessage("Failed to load settings.");
        throw std::exception();
    }
    if (proc->exitCode() != 0) {
        errorMessage("Failed to load settings.");
        throw std::exception();
    }
    QString output = proc->readAllStandardOutput();
    QJsonObject keyval = QJsonDocument::fromJson(output.toUtf8()).object();

    ui->checkBox_timeline->setChecked(keyval.value("TIMELINE_CREATE").toString("no") == "yes");
    ui->hourly->setValue(keyval.value("TIMELINE_LIMIT_HOURLY").toString("5").toUInt());
    ui->daily->setValue(keyval.value("TIMELINE_LIMIT_DAILY").toString("7").toUInt());
    ui->weekly->setValue(keyval.value("TIMELINE_LIMIT_WEEKLY").toString("0").toUInt());
    ui->monthly->setValue(keyval.value("TIMELINE_LIMIT_MONTHLY").toString("0").toUInt());
    ui->yearly->setValue(keyval.value("TIMELINE_LIMIT_YEARLY").toString("0").toUInt());
    ui->pacman->setValue(keyval.value("NUMBER_LIMIT").toString("10").toUInt());
}

Settings::~Settings()
{
    delete ui;
}

void Settings::errorMessage(QString message)
{
    QMessageBox::critical(this, "An error has occoured", message);
}

void Settings::on_buttonBox_accepted()
{
    QMap<QString, QString> new_settings;
    new_settings["TIMELINE_CREATE"] = ui->checkBox_timeline->isChecked() ? "yes" : "no";
    new_settings["TIMELINE_LIMIT_HOURLY"] = QString::number(ui->hourly->value());
    new_settings["TIMELINE_LIMIT_DAILY"] = QString::number(ui->daily->value());
    new_settings["TIMELINE_LIMIT_WEEKLY"] = QString::number(ui->weekly->value());
    new_settings["TIMELINE_LIMIT_MONTHLY"] = QString::number(ui->monthly->value());
    new_settings["TIMELINE_LIMIT_YEARLY"] = QString::number(ui->yearly->value());
    new_settings["NUMBER_LIMIT"] = QString::number(ui->pacman->value());

    QScopedPointer<QProcess> proc(new QProcess());
    QStringList params;
    params << "set-config";
    for (const auto& setting : new_settings.keys()) {
        params << setting + "=" + new_settings[setting];
    }
    proc->start("snapper", params);
    if (!proc->waitForFinished())
        errorMessage("Failed to save settings.");
    else if (proc->exitCode() != 0)
        throw std::runtime_error("Failed to save settings.");
}
