#ifndef BACKEND_H
#define BACKEND_H

#include <QDateTime>
#include <QFuture>
#include <QObject>
#include <QTemporaryDir>
#include <optional>

class SnapperException : public QException {
public:
    SnapperException(QString message)
        : message(message)
    {
    }
    void raise() const override { throw *this; }
    SnapperException* clone() const override { return new SnapperException(*this); }
    const char* what() const throw() override
    {
        return message.toUtf8().constData();
    }

private:
    QString message;
};

struct Subvolume {
    unsigned int id;
    unsigned int parent;
    QString path;
};

struct SnapshotVolume : Subvolume {
    unsigned int num;
    QDateTime date;
    QString description;
    QString type;
    bool cleanup;
    SnapshotVolume(Subvolume subvol, unsigned int num, QDateTime date, QString description, QString type, bool cleanup)
        : Subvolume(subvol)
        , num(num)
        , date(date)
        , description(description)
        , type(type)
        , cleanup(cleanup)
    {
    }
};

// Managed btrfs mount
struct ManagedMount {
    QTemporaryDir mountpoint;

public:
    explicit ManagedMount(QString uuid);
    QDir dir();
    ~ManagedMount();
};

struct SnapshotBootData {
    QString uuid;
    QString subvol;
};

class Backend : public QObject {
    Q_OBJECT
    QScopedPointer<ManagedMount> mount;
    void mountVolume();
    QMutex mutex;
    QString uuid;

public:
    explicit Backend(QObject* parent = nullptr, QString uuid_override = QString());
    // List subvolumes on currently mounted disk
    QFuture<QVector<Subvolume>> subvolumes(unsigned int timeout = 30000);
    // List snapshots under @ (+ under currently mounted disk)
    QFuture<QVector<SnapshotVolume>> snapshots(unsigned int timeout = 30000);
    // Are we currently booted off a snapshot?
    static std::optional<SnapshotBootData> snapshotBoot();
    // Restore snapshot to @
    QFuture<void> restore(SnapshotVolume to_restore, unsigned int timeout = 30000);
    // Delete snapshot
    QFuture<void> remove(SnapshotVolume to_remove, unsigned int timeout = 30000);
    QFuture<void> remove(unsigned int num, unsigned int timeout = 30000);
    // Delete subvolume
    QFuture<void> removeVolume(Subvolume volume, unsigned int timeout = 30000);
    // Create snapshot
    QFuture<void> create(QString description = "Manual Snapshot", unsigned int timeout = 30000);
    // Find snapshots older than days
    QFuture<QVector<SnapshotVolume>> findOldSnapshots(unsigned int days = 14, unsigned int timeout = 30000);
    // Find backup volumes that are older than days after a restore
    QFuture<QVector<Subvolume>> findOldRestoreVolumes(unsigned int days = 10, unsigned int timeout = 30000);
signals:
};

#endif // BACKEND_H
