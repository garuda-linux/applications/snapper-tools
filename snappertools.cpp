#include "snappertools.h"
#include "./ui_snappertools.h"
#include "backend.h"
#include "settings.h"

#include <QInputDialog>
#include <QMessageBox>
#include <QtConcurrent/QtConcurrent>

void SnapperTools::handleInitFinished(QFutureWatcher<QVector<SnapshotVolume>>* watcher)
{
    watcher->deleteLater();
    QApplication::restoreOverrideCursor();

    try {
        auto snapshots
            = watcher->result();
        // Populate the table
        ui->tableWidget->setRowCount(0);
        for (int i = snapshots.length(); i-- > 0;) {
            auto& snapshot = snapshots[i];
            auto row = ui->tableWidget->rowCount();
            ui->tableWidget->insertRow(row);
            ui->tableWidget->setVerticalHeaderItem(row, new QTableWidgetItem(QString::number(snapshot.num)));
            ui->tableWidget->setItem(row, 0, new QTableWidgetItem(snapshot.date.toString("hh:mm yyyy-MM-dd")));
            ui->tableWidget->setItem(row, 1, new QTableWidgetItem(snapshot.description));
        }
        setButtonStates(false);
    } catch (SnapperException& e) {
        errorMessage(e.what(), true);
        QApplication::exit(1);
    }
}

void SnapperTools::setButtonStates(bool busy)
{
    if (busy) {
        ui->toolButton->setDisabled(true);
        ui->toolButton_Create->setDisabled(true);
        ui->toolButton_Delete->setDisabled(true);
        ui->toolButton_Restore->setDisabled(true);
    } else {
        if (!backend->snapshotBoot()) {
            ui->toolButton_Create->setDisabled(false);
            ui->toolButton_Delete->setDisabled(false);
            ui->toolButton->setDisabled(false);
        }
        ui->toolButton_Restore->setDisabled(false);
    }
}

void SnapperTools::errorMessage(QString message, bool critical)
{
    QMessageBox::critical(this, critical ? "A critical error has occoured" : "An error has occoured", message);
}

QSet<unsigned int> SnapperTools::getSelectedSnapshots()
{
    QSet<unsigned int> list;
    auto selected = ui->tableWidget->selectedItems();
    for (auto item : selected) {
        if (item->column() != 0)
            continue;
        list.insert(ui->tableWidget->verticalHeaderItem(item->row())->text().toUInt());
    }
    return list;
}

void SnapperTools::reload()
{
    setButtonStates(true);
    auto subvolumes_future = backend->snapshots(30000);
    auto watcher = new QFutureWatcher<QVector<SnapshotVolume>>(this);
    connect(watcher, &QFutureWatcher<QVector<SnapshotVolume>>::finished, std::bind(&SnapperTools::handleInitFinished, this, watcher));
    watcher->setFuture(subvolumes_future);
}

void SnapperTools::on_toolButton_Create_clicked()
{
    bool ok;
    QString text = QInputDialog::getText(this, "Create a snapshot",
        "Snapshot description: ", QLineEdit::Normal,
        "Manual Snapshot", &ok);
    if (!ok || text.isEmpty())
        return;

    QApplication::setOverrideCursor(Qt::WaitCursor);
    setButtonStates(true);
    auto future = backend->create(text);
    auto watcher = new QFutureWatcher<void>(this);
    connect(watcher, SIGNAL(finished()), this, SLOT(reload()));
    // delete the watcher when finished too
    connect(watcher, SIGNAL(finished()), watcher, SLOT(deleteLater()));
    watcher->setFuture(future);
}

void SnapperTools::on_toolButton_Delete_clicked()
{
    QApplication::setOverrideCursor(Qt::WaitCursor);
    setButtonStates(true);
    auto selected = getSelectedSnapshots();
    auto future = QtConcurrent::run([=]() {
        QFutureSynchronizer<void> sync;
        foreach (const unsigned value, selected)
            sync.addFuture(backend->remove(value));
    });
    auto watcher = new QFutureWatcher<void>(this);
    connect(watcher, SIGNAL(finished()), this, SLOT(reload()));
    // delete the watcher when finished too
    connect(watcher, SIGNAL(finished()), watcher, SLOT(deleteLater()));
    watcher->setFuture(future);
}

void SnapperTools::on_toolButton_Restore_clicked()
{
    auto selected = getSelectedSnapshots();
    if (selected.size() > 1) {
        errorMessage("Only a single snapshot can be restored. Please select a single snapshot.");
        return;
    }
    // We can just silently fail this
    if (selected.size() < 1)
        return;
    unsigned selected_item = selected.values()[0];

    QMessageBox::StandardButton reply = QMessageBox::question(this, "Restore", "Are you sure you want to restore snapshot " + QString::number(selected_item) + " to @?",
        QMessageBox::Yes | QMessageBox::No);
    if (reply != QMessageBox::Yes)
        return;

    restore(selected_item, true);
}

SnapperTools::SnapperTools(QWidget* parent, std::optional<SnapshotBootData> data)
    : QMainWindow(parent)
    , ui(new Ui::SnapperTools)
{
    ui->setupUi(this);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
    backend = new Backend(this);
    if (data)
        restore(data->subvol, false);
    QApplication::setOverrideCursor(Qt::WaitCursor);
    reload();
}

void SnapperTools::restore(std::variant<unsigned int, QString> identifier, bool async)
{
    QApplication::setOverrideCursor(Qt::WaitCursor);
    setButtonStates(true);
    auto future = QtConcurrent::run([=]() {
        auto result = backend->snapshots().result();
        auto out = std::find_if(result.begin(), result.end(),
            [&identifier](const SnapshotVolume& x) { return std::holds_alternative<unsigned int>(identifier) ? x.num == std::get<unsigned int>(identifier) : x.path == std::get<QString>(identifier); });
        if (out == result.end()) {
            throw SnapperException("Could not find selected snapshot in snapshot list.");
        }
        backend->restore(*out).waitForFinished();
    });
    auto watcher = new QFutureWatcher<void>(this);
    connect(watcher, &QFutureWatcher<void>::finished, this, [=]() {
        QApplication::restoreOverrideCursor();
        watcher->deleteLater();
        setButtonStates(false);
        try {
            watcher->waitForFinished();
            QMessageBox::information(this, "Success!", "Subvolume has been restored successfully. Please reboot immediately.");
        } catch (SnapperException& e) {
            errorMessage(QString("Error while restoring snapshot: ") + e.what());
        }
    });
    watcher->setFuture(future);
    if (!async)
        watcher->waitForFinished();
}

SnapperTools::~SnapperTools()
{
    delete ui;
}

void SnapperTools::on_toolButton_clicked()
{
    if (!settingspage) {
        QWidget::setEnabled(false);
        try {
            settingspage = new Settings(this);
            settingspage->show();
            connect(settingspage, &QDialog::finished, this, [this]() {
                QWidget::setEnabled(true);
                settingspage->deleteLater();
                settingspage = nullptr;
            });
        } catch (...) {
            QWidget::setEnabled(true);
            settingspage->deleteLater();
            settingspage = nullptr;
        }
    }
}
