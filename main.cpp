#include "backend.h"
#include "snappertools.h"

#include <QApplication>
#include <QCommandLineParser>
#include <QMessageBox>

bool askSnapshotBoot(std::optional<SnapshotBootData>& data)
{
    QMessageBox box(QMessageBox::Question, "Snapshot boot detected", "You are currently booted into snapshot " + data->subvol + "\n\n" + "Would you like to restore it?", QMessageBox::Yes | QMessageBox::No);
    box.setWindowFlags(box.windowFlags() | Qt::WindowStaysOnTopHint);
    return box.exec() == QMessageBox::Yes;
}

QStringList parseArgs(int argc, char* argv[])
{
    QStringList list;
    const int ac = argc;
    char** const av = argv;
    for (int a = 0; a < ac; ++a) {
        list << QString::fromLocal8Bit(av[a]);
    }
    return list;
}

int main(int argc, char* argv[])
{
    QCommandLineParser cmdline;
    QCommandLineOption debug("d", "Print debug info in CLI mode");
    QCommandLineOption autostart("autostart", "Internal usage only");
    autostart.setFlags(QCommandLineOption::HiddenFromHelp);
    QCommandLineOption confirm_restore("confirm-restore", "Internal usage only");
    confirm_restore.setFlags(QCommandLineOption::HiddenFromHelp);
    QCommandLineOption help({ "h", "help" }, "Display this information");
    cmdline.addPositionalArgument("action", "Action to execute.", "gui/restore/list/delete/create/find-old/delete-old");
    cmdline.addOption(debug);
    cmdline.addOption(autostart);
    cmdline.addOption(confirm_restore);
    cmdline.addOption(help);
    cmdline.process(parseArgs(argc, argv));

    auto arguments = cmdline.positionalArguments();
    auto action = arguments.value(0, "gui");

    if (cmdline.isSet(help)) {
        action = "help";
    }

    if (action == "gui") {
        // If the application is autostarted because of a snapshot boot has been detected,
        // we should ask the user if they want to restore the snapshot *before* we ask for root
        auto snapshot_boot = Backend::snapshotBoot();
        bool shouldRestoreSnapshot = false;

        QApplication a(argc, argv);
        a.setApplicationDisplayName("Snapper Tools");
        a.setApplicationName("snapper-tools");

        // Elevate if we aren't yet
        if (geteuid() != 0) {
            if (cmdline.isSet(autostart) && (!snapshot_boot || !(shouldRestoreSnapshot = askSnapshotBoot(snapshot_boot))))
                return 0;

            auto args = QCoreApplication::arguments();
            QString cmd = "pkexec /usr/lib/snapper-tools-pkexec gui";
            if (shouldRestoreSnapshot)
                cmd += " --confirm-restore";
            for (const QString& arg : args)
                cmd += " " + arg;
            cmd += "; true";
            execlp("sh", "sh", "-c", cmd.toUtf8().constData(), NULL);
            return 1;
        } else {
            if (snapshot_boot)
                shouldRestoreSnapshot = cmdline.isSet(confirm_restore) || askSnapshotBoot(snapshot_boot);
        }
        SnapperTools w(nullptr, shouldRestoreSnapshot ? snapshot_boot : std::nullopt);
        w.show();
        return a.exec();
    }

    if (!cmdline.isSet(debug))
        qInstallMessageHandler([](QtMsgType type, const QMessageLogContext& context, const QString& msg) {});
    if (geteuid() != 0) {
        QTextStream(stdout) << "Application must be executed as root user." << Qt::endl;
        return 1;
    }

    // We implement the CLI here
    try {
        Backend backend;
        if (action == "list") {
            auto result = backend.snapshots().result();
            for (const auto& i : qAsConst(result)) {
                QTextStream(stdout) << i.num << '\t' << i.date.toString() << '\t' << i.description << Qt::endl;
            }
        } else if (action == "restore") {
            if (arguments.length() != 2) {
                QTextStream(stderr) << "Invalid number of arguments" << Qt::endl;
                return 1;
            }
            bool success;
            auto snapshot_num = arguments.at(1).toInt(&success);
            if (!success) {
                // Argument 2 can not be parsed into a number
                QTextStream(stderr) << "Snapshot number argument (2) can not be parsed into a valid number" << Qt::endl;
                return 1;
            }

            auto result = backend.snapshots().result();
            auto out = std::find_if(result.begin(), result.end(),
                [&snapshot_num](const SnapshotVolume& x) { return x.num == snapshot_num; });
            if (out == result.end()) {
                QTextStream(stderr) << "Snapshot number is not a valid snapshot" << Qt::endl;
                return 1;
            }
            backend.restore(*out).waitForFinished();
            return 0;
        } else if (action == "delete") {
            if (arguments.length() != 2) {
                QTextStream(stderr) << "Invalid number of arguments" << Qt::endl;
                return 1;
            }
            bool success;
            auto snapshot_num = arguments.at(1).toInt(&success);
            if (!success) {
                // Argument 2 can not be parsed into a number
                QTextStream(stderr) << "Snapshot number argument (2) can not be parsed into a valid number" << Qt::endl;
                return 1;
            }

            auto result = backend.snapshots().result();
            auto out = std::find_if(result.begin(), result.end(),
                [&snapshot_num](const SnapshotVolume& x) { return x.num == snapshot_num; });
            if (out == result.end()) {
                QTextStream(stderr) << "Snapshot number is not a valid snapshot" << Qt::endl;
                return 1;
            }
            backend.remove(*out).waitForFinished();
            return 0;
        } else if (action == "create") {
            if (arguments.length() < 1 || arguments.length() > 2) {
                QTextStream(stderr) << "Invalid number of arguments" << Qt::endl;
                return 1;
            }

            backend.create(arguments.length() == 2 ? arguments.at(1) : "Manual Snapshot").waitForFinished();
            return 0;
        } else if (action == "find-old") {
            bool found = false;
            // Find old snapshots that don't have any kind of auto cleanup
            auto snapshots = backend.findOldSnapshots().result();
            if (!snapshots.isEmpty()) {
                found = true;
                QTextStream(stdout) << "Old snapshots:" << Qt::endl;
                for (const auto& i : qAsConst(snapshots)) {
                    QTextStream(stdout) << i.num << '\t' << i.date.toString() << '\t' << i.description << Qt::endl;
                }
            }
            auto subvolumes = backend.findOldRestoreVolumes().result();
            if (!subvolumes.isEmpty()) {
                found = true;
                QTextStream(stdout) << "Old restore subvolumes:" << Qt::endl;
                for (const auto& i : qAsConst(subvolumes)) {
                    QTextStream(stdout) << i.path << Qt::endl;
                }
            }
            if (found)
                return 0;
        } else if (action == "delete-old") {
            bool found = false;
            // Find old snapshots that don't have any kind of auto cleanup
            auto snapshots = backend.findOldSnapshots().result();
            for (const auto& i : qAsConst(snapshots)) {
                backend.remove(i).waitForFinished();
                found = true;
            }
            auto subvolumes = backend.findOldRestoreVolumes().result();
            for (const auto& i : qAsConst(subvolumes)) {
                backend.removeVolume(i).waitForFinished();
                found = true;
            }
            if (found)
                return 0;
        } else {
            QCoreApplication a(argc, argv);
            a.setApplicationName("snapper-tools");
            cmdline.showHelp(cmdline.isSet(help) ? 0 : 1);
        }
    } catch (SnapperException& exception) {
        QTextStream(stdout) << "Error while running action " + action + ": " + exception.what() << Qt::endl;
    }
    return 1;
}
