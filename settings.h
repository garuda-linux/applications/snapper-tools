#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>
#include <QMap>

namespace Ui {
class Settings;
}

class Settings : public QDialog {
    Q_OBJECT

public:
    explicit Settings(QWidget* parent = nullptr);
    ~Settings();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::Settings* ui;
    QMap<QString, QString> settingsmap;
    void errorMessage(QString message);
};

#endif // SETTINGS_H
