#ifndef SNAPPERTOOLS_H
#define SNAPPERTOOLS_H

#include "backend.h"

#include <QFutureWatcher>
#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui {
class SnapperTools;
}
QT_END_NAMESPACE

class Settings;

class SnapperTools : public QMainWindow {
    Q_OBJECT
public:
    SnapperTools(QWidget* parent = nullptr, std::optional<SnapshotBootData> data = std::nullopt);
    void restore(std::variant<unsigned int, QString> identifier, bool async);
    ~SnapperTools();

private slots:
    void handleInitFinished(QFutureWatcher<QVector<SnapshotVolume>>* watcher);
    void on_toolButton_Create_clicked();
    void reload();
    void on_toolButton_Delete_clicked();
    void on_toolButton_Restore_clicked();
    void on_toolButton_clicked();

private:
    Ui::SnapperTools* ui;
    Backend* backend;
    Settings* settingspage = nullptr;
    void setButtonStates(bool busy);
    void errorMessage(QString message, bool critical = false);
    QSet<unsigned int> getSelectedSnapshots();
};
#endif // SNAPPERTOOLS_H
