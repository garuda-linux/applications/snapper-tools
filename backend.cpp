#include "backend.h"
#include <QDebug>
#include <QProcess>
#include <QtConcurrent/QtConcurrent>
#include <fcntl.h>
#include <stdio.h>
#include <sys/mount.h>

Backend::Backend(QObject* parent, QString uuid_override)
    : QObject { parent }
    , uuid(uuid_override)
{
}

void Backend::mountVolume()
{
    if (mount)
        return;
    if (uuid.isNull()) {
        auto snapshot_boot = snapshotBoot();
        if (snapshot_boot)
            uuid = snapshot_boot->uuid;
        else {
            QScopedPointer<QProcess> proc(new QProcess());
            proc->start("btrfs", QStringList() << "filesystem"
                                               << "show"
                                               << "-m");
            if (!proc->waitForFinished())
                throw SnapperException("Timed out while reading filesystem list");
            if (proc->exitCode() != 0)
                throw SnapperException("Failed to retrieve filesystem list");
            auto out = QString::fromUtf8(proc->readAllStandardOutput());
            static QRegularExpression re("Label: .*  uuid: (.*)");
            QRegularExpressionMatch match = re.match(out);
            if (!match.hasMatch())
                throw SnapperException("Failed to find UUID of mounted btrfs volume");
            uuid = match.captured(1);
        }
    }
    qDebug() << uuid;
    mount.reset(new ManagedMount(uuid));
}

QFuture<QVector<Subvolume>> Backend::subvolumes(unsigned int timeout)
{
    return QtConcurrent::run([timeout, this] {
        QMutexLocker lock(&mutex);
        mountVolume();
        QScopedPointer<QProcess> proc(new QProcess());
        proc->start("btrfs", QStringList() << "subvolume"
                                           << "list" << mount->dir().absolutePath());
        if (!proc->waitForFinished(timeout))
            throw SnapperException("Timed out while reading subvolume list");
        if (proc->exitCode() != 0)
            throw SnapperException("Failed to read subvolume list");
        QVector<Subvolume> subvolumes;
        while (proc->canReadLine()) {
            auto line = QString::fromUtf8(proc->readLine());
            auto subvolume = line.simplified().split(" ");
            if (subvolume.length() == 9)
                subvolumes.append({ subvolume.at(1).toUInt(), subvolume.at(6).toUInt(), subvolume.at(8) });
        }
        return subvolumes;
    });
}

QFuture<QVector<SnapshotVolume>> Backend::snapshots(unsigned int timeout)
{
    return QtConcurrent::run([timeout, this] {
        auto volumes = subvolumes().result();
        QMutexLocker lock(&mutex);
        QVector<SnapshotVolume> snapshots;
        for (const auto& volume : qAsConst(volumes)) {
            static QRegularExpression re("@\\/.snapshots\\/(\\d+)\\/snapshot");
            QRegularExpressionMatch match = re.match(volume.path);
            if (!match.hasMatch())
                continue;
            auto num = match.captured(1).toUInt();
            QFile info_file(QFileInfo(mount->dir().filePath(volume.path)).dir().filePath("info.xml"));
            if (!info_file.open(QIODevice::ReadOnly)) {
                qDebug() << "Failed to read " + info_file.fileName();
                continue;
            }
            QXmlStreamReader xml(&info_file);
            QString description;
            QDateTime date;
            QString type;
            bool cleanup = false;
            xml.readNextStartElement();
            while (!xml.atEnd()) {
                xml.readNextStartElement();
                if (xml.name() == "snapshot") {
                    xml.readNextStartElement();
                    continue;
                } else if (xml.name() == "date") {
                    date = QDateTime::fromString(xml.readElementText(), Qt::ISODate);
                    date.setTimeSpec(Qt::UTC);
                } else if (xml.name() == "description")
                    description = xml.readElementText();
                else if (xml.name() == "type")
                    type = xml.readElementText();
                else if (xml.name() == "cleanup") {
                    cleanup = true;
                    xml.skipCurrentElement();
                } else
                    xml.skipCurrentElement();
            }
            snapshots.append({ volume,
                num,
                date.toLocalTime(),
                description,
                type,
                cleanup });
        }
        return snapshots;
    });
}

QFuture<QVector<SnapshotVolume>> Backend::findOldSnapshots(unsigned int days, unsigned int timeout)
{
    return QtConcurrent::run([timeout, this, days] {
        auto volumes = snapshots(timeout).result();
        QMutexLocker lock(&mutex);
        volumes.erase(std::remove_if(volumes.begin(), volumes.end(), [&days](SnapshotVolume& i) { return i.cleanup || i.date.daysTo(QDateTime::currentDateTime()) < days; }),
            volumes.end());
        return volumes;
    });
}

QFuture<QVector<Subvolume>> Backend::findOldRestoreVolumes(unsigned int days, unsigned int timeout)
{
    return QtConcurrent::run([timeout, this, days] {
        auto volumes = subvolumes(timeout).result();
        QMutexLocker lock(&mutex);
        volumes.erase(std::remove_if(volumes.begin(), volumes.end(), [&days, this](Subvolume& i) {
            static QRegularExpression re("(@_backup_[0-9]+)|(restore_backup_\\S+_[0-9]+$)");
            QRegularExpressionMatch match = re.match(i.path);
            if (!match.hasMatch())
                return true;
            const QDateTime lastModified = QFileInfo(mount->dir().filePath(i.path)).lastModified();
            return lastModified.daysTo(QDateTime::currentDateTime()) < days;
        }),
            volumes.end());
        return volumes;
    });
}

std::optional<SnapshotBootData> Backend::snapshotBoot()
{
    QFile inputFile("/proc/cmdline");
    if (!inputFile.open(QIODevice::ReadOnly))
        return std::nullopt;
    QTextStream in(&inputFile);
    QString cmdline = in.readAll();
    QString uuid = QRegularExpression("(?<=root=UUID=)([\\S]*)").match(cmdline).captured(0);
    QString subvol = QRegularExpression("rootflags=.*subvol=([^,|\\s]*)").match(cmdline).captured(1);
    if (uuid.isNull() || subvol.isNull() || !subvol.contains(".snapshots"))
        return std::nullopt;
    return SnapshotBootData { uuid, subvol };
}

QFuture<void> Backend::restore(SnapshotVolume to_restore, unsigned int timeout)
{
    return QtConcurrent::run([to_restore, timeout, this] {
        auto volumes = subvolumes().result();
        QMutexLocker lock(&mutex);
        mountVolume();
        auto rootDir = mount->dir();
        if (!rootDir.exists())
            throw SnapperException("Unknown error");
        // We use the same format as btrfs-assistant here to allow for interoperability to some degree
        QString target = "@_backup_" + QDateTime::currentDateTime().toString("yyyyddMMHHmmsszzz");
        QScopedPointer<QProcess> proc(new QProcess());
        proc->start("btrfs", QStringList() << "subvolume"
                                           << "snapshot" << rootDir.filePath(to_restore.path) << rootDir.filePath(target));
        if (!proc->waitForFinished(timeout))
            throw SnapperException("Timed out while creating copy of snapshot");
        if (proc->exitCode() != 0)
            throw std::runtime_error("Failed to create copy of snapshot");

        // Find the root (mounted at /) subvolume (@)
        auto rootSubvol = std::find_if(volumes.begin(), volumes.end(),
            [](const Subvolume& x) { return x.path == "@"; });
        if (rootSubvol == volumes.end())
            throw SnapperException("@ subvolume not found in list of subvolumes for BTRFS volume UUID=" + uuid);

        auto rootSubvolDir = QDir(rootDir.filePath(rootSubvol->path));
        for (const auto& volume : qAsConst(volumes)) {
            // Find child subvolumes
            if (rootSubvol->id == volume.parent) {
                // Now we move all child subvolumes to the new volume
                auto source = rootDir.filePath(volume.path);
                auto dest = QDir(rootDir.filePath(target)).filePath(rootSubvolDir.relativeFilePath(source));
                qDebug() << source << dest;

                if (QFile::exists(dest) && !QDir(dest).removeRecursively())
                    throw SnapperException("Failed to delete existing directory from snapshot.");

                if (!QFile::rename(source, dest))
                    throw SnapperException("Unknown error while moving subvolume to snapshot");
            }
        }

        // We now swap the target directory with @ atomically
        if (renameat2(0, rootDir.filePath(target).toUtf8().constData(), 0, rootDir.filePath(rootSubvol->path).toUtf8().constData(), RENAME_EXCHANGE) != 0)
            throw SnapperException("Unknown error while restoring snapshot");
    });
}

QFuture<void> Backend::remove(unsigned int num, unsigned int timeout)
{
    return QtConcurrent::run([timeout, num] {
        qDebug() << "Deleting:" << num;
        QScopedPointer<QProcess> proc(new QProcess());
        proc->start("snapper", QStringList() << "delete" << QString::number(num));
        if (!proc->waitForFinished(timeout))
            throw SnapperException("Timed out while deleting snapshot");
        if (proc->exitCode() != 0)
            throw SnapperException("Unknown error while deleting snapshot");
    });
}

QFuture<void> Backend::remove(SnapshotVolume to_remove, unsigned int timeout)
{
    return remove(to_remove.num, timeout);
}

QFuture<void> Backend::removeVolume(Subvolume volume, unsigned int timeout)
{
    return QtConcurrent::run([timeout, volume, this] {
        qDebug() << "Deleting:" << mount->dir().filePath(volume.path);
        QScopedPointer<QProcess> proc(new QProcess());
        proc->start("btrfs", QStringList() << "subvolume"
                                           << "delete" << mount->dir().filePath(volume.path));
        if (!proc->waitForFinished(timeout))
            throw SnapperException("Timed out while creating copy of snapshot");
        if (proc->exitCode() != 0)
            throw std::runtime_error("Failed to create copy of snapshot");
    });
}

QFuture<void> Backend::create(QString description, unsigned int timeout)
{
    return QtConcurrent::run([timeout, description] {
        QScopedPointer<QProcess> proc(new QProcess());
        auto args = QStringList() << "create"
                                  << "-t"
                                  << "single";
        if (!description.isNull())
            args << "-d" << description;
        proc->start("snapper", args);
        if (!proc->waitForFinished(timeout))
            throw SnapperException("Timed out while creating snapshot");
        if (proc->exitCode() != 0)
            throw SnapperException("Unknown error while creating snapshot");
    });
}

ManagedMount::ManagedMount(QString uuid)
    : mountpoint("/run/snapper-tools-XXXXXX")
{
    qDebug() << "Mounting " + uuid;
    QFileInfo volume(QDir("/dev/disk/by-uuid"), uuid);
    if (volume.exists() && mountpoint.isValid())
        if (mount(volume.absoluteFilePath().toUtf8().constData(), mountpoint.path().toUtf8().constData(), "btrfs", MS_SYNCHRONOUS, "subvolid=5") != 0)
            throw SnapperException("Failed to mount volume UUID=" + uuid);
}

QDir ManagedMount::dir()
{
    return QDir(mountpoint.path());
}

ManagedMount::~ManagedMount()
{
    qDebug() << "Unmounting  " + mountpoint.path();
    umount2(mountpoint.path().toUtf8().constData(), MNT_DETACH);
    mountpoint.remove();
}
